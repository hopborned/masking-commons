package jdbc.connections

import javax.sql.DataSource

interface DataSourceFactory {

    fun dataSource(jdbcUrl: String, poolSize: Int): DataSource

    fun dataSource(jdbcUrl: String): DataSource {
        return dataSource(jdbcUrl, 1)
    }

}
