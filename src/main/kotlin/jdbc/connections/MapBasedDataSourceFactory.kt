package jdbc.connections

import com.zaxxer.hikari.HikariConfig
import com.zaxxer.hikari.HikariDataSource
import java.util.*
import javax.sql.DataSource

class MapBasedDataSourceFactory : DataSourceFactory {

    private val availableConnections: MutableMap<String, DataSource> = mutableMapOf()

    @Synchronized
    override fun dataSource(jdbcUrl: String, poolSize: Int): DataSource {
        val dataSource = availableConnections[jdbcUrl]
        return if (dataSource == null) {
            val hikariDataSource = HikariDataSource(HikariConfig(properties(jdbcUrl, poolSize)))
            availableConnections[jdbcUrl] = hikariDataSource
            hikariDataSource
        } else {
            dataSource
        }
    }

    private fun properties(jdbcUrl: String, poolSize: Int): Properties {
        val properties = Properties()
        properties.setProperty("jdbcUrl", jdbcUrl)
        properties.setProperty("maximumPoolSize", poolSize.toString())
        return properties
    }
}